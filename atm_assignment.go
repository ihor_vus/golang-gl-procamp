package main

import (
    "fmt"
    "strconv"
)

func main() {
    // put your code here
    menu := [...]string{1:"Info", 2: "Deposit", 3: "Withdraw", 4: "Exit"}
    for {
	   // print mune
        fmt.Println("Choose next operation:")
        for i:=1; i <= 4; i++ {
            fmt.Printf("%d. %s.\n", i, menu[i])
        }
		//read response
        var resp string
        fmt.Scanf("%s", &resp)
        switch resp {
           case "1","2","3":
            i, _ := strconv.Atoi(resp)
            fmt.Println(menu[i])
           case "4":    // exit
            fmt.Println("Bye!")
            return
           default:
            fmt.Println("Wrong input")
        }
          
    }
    
}